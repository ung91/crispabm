package ungzombies;

import static repast.simphony.relogo.Utility.*;
import static repast.simphony.relogo.UtilityG.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import groovy.lang.Closure;
import repast.simphony.relogo.*;
import repast.simphony.relogo.builder.GeneratedByReLogoBuilder;
import repast.simphony.relogo.builder.ReLogoBuilderGeneratedFor;

@GeneratedByReLogoBuilder
@SuppressWarnings({"unused","rawtypes","unchecked"})
public class ReLogoObserver extends BaseObserver{

	/**
	 * Makes a number of randomly oriented humans and then executes a set of commands on the
	 * created humans.
	 * 
	 * @param number
	 *            a number
	 * @param closure
	 *            a set of commands
	 * @return created humans
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Human")
	public AgentSet<ungzombies.relogo.Human> createHumans(int number, Closure closure) {
		AgentSet<ungzombies.relogo.Human> result = new AgentSet<>();
		AgentSet<Turtle> createResult = this.crt(number,closure,"Human");
		for (Turtle t : createResult){
			if (t instanceof ungzombies.relogo.Human){
				result.add((ungzombies.relogo.Human)t);
			}
		} 
		return result; 
	}

	/**
	 * Makes a number of randomly oriented humans and then executes a set of commands on the
	 * created humans.
	 * 
	 * @param number
	 *            a number
	 * @param closure
	 *            a set of commands
	 * @return created humans
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Human")
	public AgentSet<ungzombies.relogo.Human> createHumans(int number) {
		return createHumans(number,null);
	}

	/**
	 * Makes a number of uniformly fanned humans and then executes a set of commands on the
	 * created humans.
	 * 
	 * @param number
	 *            a number
	 * @param closure
	 *            a set of commands
	 * @return created humans
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Human")
	public AgentSet<ungzombies.relogo.Human> createOrderedHumans(int number, Closure closure) {
		AgentSet<ungzombies.relogo.Human> result = new AgentSet<>();
		AgentSet<Turtle> createResult = this.cro(number,closure,"Human");
		for (Turtle t : createResult){
			if (t instanceof ungzombies.relogo.Human){
				result.add((ungzombies.relogo.Human)t);
			}
		} 
		return result; 
	}

	/**
	 * Makes a number of uniformly fanned humans and then executes a set of commands on the
	 * created humans.
	 * 
	 * @param number
	 *            a number
	 * @param closure
	 *            a set of commands
	 * @return created humans
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Human")
	public AgentSet<ungzombies.relogo.Human> createOrderedHumans(int number) {
		return createOrderedHumans(number,null);
	}

	/**
	 * Queries if object is a human.
	 * 
	 * @param o
	 *            an object
	 * @return true or false based on whether the object is a human
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Human")
	public boolean isHumanQ(Object o){
		return (o instanceof ungzombies.relogo.Human);
	}

	/**
	 * Returns an agentset containing all humans.
	 * 
	 * @return agentset of all humans
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Human")
	public AgentSet<ungzombies.relogo.Human> humans(){
		AgentSet<ungzombies.relogo.Human> a = new AgentSet<ungzombies.relogo.Human>();
		for (Object e : this.getContext().getObjects(ungzombies.relogo.Human.class)) {
			if (e instanceof ungzombies.relogo.Human){
				a.add((ungzombies.relogo.Human)e);
			}
		}
		return a;
	}

	/**
	 * Returns the human with the given who number.
	 * 
	 * @param number
	 *            a number
	 * @return turtle number
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Human")
	public ungzombies.relogo.Human human(Number number){
		Turtle turtle = Utility.turtleU(number.intValue(), this);
		if (turtle instanceof ungzombies.relogo.Human)
			return (ungzombies.relogo.Human) turtle;
		return null;
	}

	/**
	 * Returns an agentset of humans on a given patch.
	 * 
	 * @param p
	 *            a patch
	 * @return agentset of humans on patch p
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Human")
	public AgentSet<ungzombies.relogo.Human> humansOn(Patch p){
		AgentSet<ungzombies.relogo.Human> result = new AgentSet<ungzombies.relogo.Human>();						
		for (Turtle t : Utility.getTurtlesOnGridPoint(p.getGridLocation(),this,"human")){
			if (t instanceof ungzombies.relogo.Human)
			result.add((ungzombies.relogo.Human)t);
		}
		return result;
	}

	/**
	 * Returns an agentset of humans on the same patch as a turtle.
	 * 
	 * @param t
	 *            a turtle
	 * @return agentset of humans on the same patch as turtle t
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Human")
	public AgentSet<ungzombies.relogo.Human> humansOn(Turtle t){
		AgentSet<ungzombies.relogo.Human> result = new AgentSet<ungzombies.relogo.Human>();						
		for (Turtle tt : Utility.getTurtlesOnGridPoint(Utility.ndPointToGridPoint(t.getTurtleLocation()),this,"human")){
			if (tt instanceof ungzombies.relogo.Human)
			result.add((ungzombies.relogo.Human)tt);
		}
		return result;
	}

	/**
	 * Returns an agentset of humans on the patches in a collection or on the patches
	 * that a collection of turtles are.
	 * 
	 * @param a
	 *            a collection
	 * @return agentset of humans on the patches in collection a or on the patches
	 *         that collection a turtles are
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Human")
	public AgentSet<ungzombies.relogo.Human> humansOn(Collection c){

		if (c == null || c.isEmpty()){
			return new AgentSet<ungzombies.relogo.Human>();
		}

		Set<ungzombies.relogo.Human> total = new HashSet<ungzombies.relogo.Human>();
		if (c.iterator().next() instanceof Turtle){
			for (Object o : c){
				if (o instanceof Turtle){
					Turtle t = (Turtle) o;
					total.addAll(humansOn(t));
				}
			}
		}
		else {
			for (Object o : c){
				if (o instanceof Patch){
					Patch p = (Patch) o;
					total.addAll(humansOn(p));
				}
			}
		}
		return new AgentSet<ungzombies.relogo.Human>(total);
	}

	/**
	 * Makes a number of randomly oriented userTurtles and then executes a set of commands on the
	 * created userTurtles.
	 * 
	 * @param number
	 *            a number
	 * @param closure
	 *            a set of commands
	 * @return created userTurtles
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.UserTurtle")
	public AgentSet<ungzombies.relogo.UserTurtle> createUserTurtles(int number, Closure closure) {
		AgentSet<ungzombies.relogo.UserTurtle> result = new AgentSet<>();
		AgentSet<Turtle> createResult = this.crt(number,closure,"UserTurtle");
		for (Turtle t : createResult){
			if (t instanceof ungzombies.relogo.UserTurtle){
				result.add((ungzombies.relogo.UserTurtle)t);
			}
		} 
		return result; 
	}

	/**
	 * Makes a number of randomly oriented userTurtles and then executes a set of commands on the
	 * created userTurtles.
	 * 
	 * @param number
	 *            a number
	 * @param closure
	 *            a set of commands
	 * @return created userTurtles
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.UserTurtle")
	public AgentSet<ungzombies.relogo.UserTurtle> createUserTurtles(int number) {
		return createUserTurtles(number,null);
	}

	/**
	 * Makes a number of uniformly fanned userTurtles and then executes a set of commands on the
	 * created userTurtles.
	 * 
	 * @param number
	 *            a number
	 * @param closure
	 *            a set of commands
	 * @return created userTurtles
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.UserTurtle")
	public AgentSet<ungzombies.relogo.UserTurtle> createOrderedUserTurtles(int number, Closure closure) {
		AgentSet<ungzombies.relogo.UserTurtle> result = new AgentSet<>();
		AgentSet<Turtle> createResult = this.cro(number,closure,"UserTurtle");
		for (Turtle t : createResult){
			if (t instanceof ungzombies.relogo.UserTurtle){
				result.add((ungzombies.relogo.UserTurtle)t);
			}
		} 
		return result; 
	}

	/**
	 * Makes a number of uniformly fanned userTurtles and then executes a set of commands on the
	 * created userTurtles.
	 * 
	 * @param number
	 *            a number
	 * @param closure
	 *            a set of commands
	 * @return created userTurtles
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.UserTurtle")
	public AgentSet<ungzombies.relogo.UserTurtle> createOrderedUserTurtles(int number) {
		return createOrderedUserTurtles(number,null);
	}

	/**
	 * Queries if object is a userTurtle.
	 * 
	 * @param o
	 *            an object
	 * @return true or false based on whether the object is a userTurtle
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.UserTurtle")
	public boolean isUserTurtleQ(Object o){
		return (o instanceof ungzombies.relogo.UserTurtle);
	}

	/**
	 * Returns an agentset containing all userTurtles.
	 * 
	 * @return agentset of all userTurtles
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.UserTurtle")
	public AgentSet<ungzombies.relogo.UserTurtle> userTurtles(){
		AgentSet<ungzombies.relogo.UserTurtle> a = new AgentSet<ungzombies.relogo.UserTurtle>();
		for (Object e : this.getContext().getObjects(ungzombies.relogo.UserTurtle.class)) {
			if (e instanceof ungzombies.relogo.UserTurtle){
				a.add((ungzombies.relogo.UserTurtle)e);
			}
		}
		return a;
	}

	/**
	 * Returns the userTurtle with the given who number.
	 * 
	 * @param number
	 *            a number
	 * @return turtle number
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.UserTurtle")
	public ungzombies.relogo.UserTurtle userTurtle(Number number){
		Turtle turtle = Utility.turtleU(number.intValue(), this);
		if (turtle instanceof ungzombies.relogo.UserTurtle)
			return (ungzombies.relogo.UserTurtle) turtle;
		return null;
	}

	/**
	 * Returns an agentset of userTurtles on a given patch.
	 * 
	 * @param p
	 *            a patch
	 * @return agentset of userTurtles on patch p
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.UserTurtle")
	public AgentSet<ungzombies.relogo.UserTurtle> userTurtlesOn(Patch p){
		AgentSet<ungzombies.relogo.UserTurtle> result = new AgentSet<ungzombies.relogo.UserTurtle>();						
		for (Turtle t : Utility.getTurtlesOnGridPoint(p.getGridLocation(),this,"userTurtle")){
			if (t instanceof ungzombies.relogo.UserTurtle)
			result.add((ungzombies.relogo.UserTurtle)t);
		}
		return result;
	}

	/**
	 * Returns an agentset of userTurtles on the same patch as a turtle.
	 * 
	 * @param t
	 *            a turtle
	 * @return agentset of userTurtles on the same patch as turtle t
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.UserTurtle")
	public AgentSet<ungzombies.relogo.UserTurtle> userTurtlesOn(Turtle t){
		AgentSet<ungzombies.relogo.UserTurtle> result = new AgentSet<ungzombies.relogo.UserTurtle>();						
		for (Turtle tt : Utility.getTurtlesOnGridPoint(Utility.ndPointToGridPoint(t.getTurtleLocation()),this,"userTurtle")){
			if (tt instanceof ungzombies.relogo.UserTurtle)
			result.add((ungzombies.relogo.UserTurtle)tt);
		}
		return result;
	}

	/**
	 * Returns an agentset of userTurtles on the patches in a collection or on the patches
	 * that a collection of turtles are.
	 * 
	 * @param a
	 *            a collection
	 * @return agentset of userTurtles on the patches in collection a or on the patches
	 *         that collection a turtles are
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.UserTurtle")
	public AgentSet<ungzombies.relogo.UserTurtle> userTurtlesOn(Collection c){

		if (c == null || c.isEmpty()){
			return new AgentSet<ungzombies.relogo.UserTurtle>();
		}

		Set<ungzombies.relogo.UserTurtle> total = new HashSet<ungzombies.relogo.UserTurtle>();
		if (c.iterator().next() instanceof Turtle){
			for (Object o : c){
				if (o instanceof Turtle){
					Turtle t = (Turtle) o;
					total.addAll(userTurtlesOn(t));
				}
			}
		}
		else {
			for (Object o : c){
				if (o instanceof Patch){
					Patch p = (Patch) o;
					total.addAll(userTurtlesOn(p));
				}
			}
		}
		return new AgentSet<ungzombies.relogo.UserTurtle>(total);
	}

	/**
	 * Makes a number of randomly oriented zombies and then executes a set of commands on the
	 * created zombies.
	 * 
	 * @param number
	 *            a number
	 * @param closure
	 *            a set of commands
	 * @return created zombies
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Zombie")
	public AgentSet<ungzombies.relogo.Zombie> createZombies(int number, Closure closure) {
		AgentSet<ungzombies.relogo.Zombie> result = new AgentSet<>();
		AgentSet<Turtle> createResult = this.crt(number,closure,"Zombie");
		for (Turtle t : createResult){
			if (t instanceof ungzombies.relogo.Zombie){
				result.add((ungzombies.relogo.Zombie)t);
			}
		} 
		return result; 
	}

	/**
	 * Makes a number of randomly oriented zombies and then executes a set of commands on the
	 * created zombies.
	 * 
	 * @param number
	 *            a number
	 * @param closure
	 *            a set of commands
	 * @return created zombies
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Zombie")
	public AgentSet<ungzombies.relogo.Zombie> createZombies(int number) {
		return createZombies(number,null);
	}

	/**
	 * Makes a number of uniformly fanned zombies and then executes a set of commands on the
	 * created zombies.
	 * 
	 * @param number
	 *            a number
	 * @param closure
	 *            a set of commands
	 * @return created zombies
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Zombie")
	public AgentSet<ungzombies.relogo.Zombie> createOrderedZombies(int number, Closure closure) {
		AgentSet<ungzombies.relogo.Zombie> result = new AgentSet<>();
		AgentSet<Turtle> createResult = this.cro(number,closure,"Zombie");
		for (Turtle t : createResult){
			if (t instanceof ungzombies.relogo.Zombie){
				result.add((ungzombies.relogo.Zombie)t);
			}
		} 
		return result; 
	}

	/**
	 * Makes a number of uniformly fanned zombies and then executes a set of commands on the
	 * created zombies.
	 * 
	 * @param number
	 *            a number
	 * @param closure
	 *            a set of commands
	 * @return created zombies
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Zombie")
	public AgentSet<ungzombies.relogo.Zombie> createOrderedZombies(int number) {
		return createOrderedZombies(number,null);
	}

	/**
	 * Queries if object is a zombie.
	 * 
	 * @param o
	 *            an object
	 * @return true or false based on whether the object is a zombie
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Zombie")
	public boolean isZombieQ(Object o){
		return (o instanceof ungzombies.relogo.Zombie);
	}

	/**
	 * Returns an agentset containing all zombies.
	 * 
	 * @return agentset of all zombies
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Zombie")
	public AgentSet<ungzombies.relogo.Zombie> zombies(){
		AgentSet<ungzombies.relogo.Zombie> a = new AgentSet<ungzombies.relogo.Zombie>();
		for (Object e : this.getContext().getObjects(ungzombies.relogo.Zombie.class)) {
			if (e instanceof ungzombies.relogo.Zombie){
				a.add((ungzombies.relogo.Zombie)e);
			}
		}
		return a;
	}

	/**
	 * Returns the zombie with the given who number.
	 * 
	 * @param number
	 *            a number
	 * @return turtle number
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Zombie")
	public ungzombies.relogo.Zombie zombie(Number number){
		Turtle turtle = Utility.turtleU(number.intValue(), this);
		if (turtle instanceof ungzombies.relogo.Zombie)
			return (ungzombies.relogo.Zombie) turtle;
		return null;
	}

	/**
	 * Returns an agentset of zombies on a given patch.
	 * 
	 * @param p
	 *            a patch
	 * @return agentset of zombies on patch p
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Zombie")
	public AgentSet<ungzombies.relogo.Zombie> zombiesOn(Patch p){
		AgentSet<ungzombies.relogo.Zombie> result = new AgentSet<ungzombies.relogo.Zombie>();						
		for (Turtle t : Utility.getTurtlesOnGridPoint(p.getGridLocation(),this,"zombie")){
			if (t instanceof ungzombies.relogo.Zombie)
			result.add((ungzombies.relogo.Zombie)t);
		}
		return result;
	}

	/**
	 * Returns an agentset of zombies on the same patch as a turtle.
	 * 
	 * @param t
	 *            a turtle
	 * @return agentset of zombies on the same patch as turtle t
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Zombie")
	public AgentSet<ungzombies.relogo.Zombie> zombiesOn(Turtle t){
		AgentSet<ungzombies.relogo.Zombie> result = new AgentSet<ungzombies.relogo.Zombie>();						
		for (Turtle tt : Utility.getTurtlesOnGridPoint(Utility.ndPointToGridPoint(t.getTurtleLocation()),this,"zombie")){
			if (tt instanceof ungzombies.relogo.Zombie)
			result.add((ungzombies.relogo.Zombie)tt);
		}
		return result;
	}

	/**
	 * Returns an agentset of zombies on the patches in a collection or on the patches
	 * that a collection of turtles are.
	 * 
	 * @param a
	 *            a collection
	 * @return agentset of zombies on the patches in collection a or on the patches
	 *         that collection a turtles are
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Zombie")
	public AgentSet<ungzombies.relogo.Zombie> zombiesOn(Collection c){

		if (c == null || c.isEmpty()){
			return new AgentSet<ungzombies.relogo.Zombie>();
		}

		Set<ungzombies.relogo.Zombie> total = new HashSet<ungzombies.relogo.Zombie>();
		if (c.iterator().next() instanceof Turtle){
			for (Object o : c){
				if (o instanceof Turtle){
					Turtle t = (Turtle) o;
					total.addAll(zombiesOn(t));
				}
			}
		}
		else {
			for (Object o : c){
				if (o instanceof Patch){
					Patch p = (Patch) o;
					total.addAll(zombiesOn(p));
				}
			}
		}
		return new AgentSet<ungzombies.relogo.Zombie>(total);
	}

	/**
	 * Queries if object is a infection.
	 * 
	 * @param o
	 *            an object
	 * @return true or false based on whether the object is a infection
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Infection")
	public boolean isInfectionQ(Object o){
		return (o instanceof ungzombies.relogo.Infection);
	}

	/**
	 * Returns an agentset containing all infections.
	 * 
	 * @return agentset of all infections
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Infection")
	public AgentSet<ungzombies.relogo.Infection> infections(){
		AgentSet<ungzombies.relogo.Infection> a = new AgentSet<ungzombies.relogo.Infection>();
		for (Object e : this.getContext().getObjects(ungzombies.relogo.Infection.class)) {
			if (e instanceof ungzombies.relogo.Infection){
				a.add((ungzombies.relogo.Infection)e);
			}
		}
		return a;
	}

	/**
	 * Returns the infection between two turtles.
	 * 
	 * @param oneEnd
	 *            an integer
	 * @param otherEnd
	 *            an integer
	 * @return infection between two turtles
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Infection")
	public ungzombies.relogo.Infection infection(Number oneEnd, Number otherEnd) {
		return (ungzombies.relogo.Infection)(this.getNetwork("Infection").getEdge(turtle(oneEnd),turtle(otherEnd)));
	}

	/**
	 * Returns the infection between two turtles.
	 * 
	 * @param oneEnd
	 *            a turtle
	 * @param otherEnd
	 *            a turtle
	 * @return infection between two turtles
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.Infection")
	public ungzombies.relogo.Infection infection(Turtle oneEnd, Turtle otherEnd) {
		return infection(oneEnd.getWho(), otherEnd.getWho());
	}

	/**
	 * Queries if object is a userLink.
	 * 
	 * @param o
	 *            an object
	 * @return true or false based on whether the object is a userLink
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.UserLink")
	public boolean isUserLinkQ(Object o){
		return (o instanceof ungzombies.relogo.UserLink);
	}

	/**
	 * Returns an agentset containing all userLinks.
	 * 
	 * @return agentset of all userLinks
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.UserLink")
	public AgentSet<ungzombies.relogo.UserLink> userLinks(){
		AgentSet<ungzombies.relogo.UserLink> a = new AgentSet<ungzombies.relogo.UserLink>();
		for (Object e : this.getContext().getObjects(ungzombies.relogo.UserLink.class)) {
			if (e instanceof ungzombies.relogo.UserLink){
				a.add((ungzombies.relogo.UserLink)e);
			}
		}
		return a;
	}

	/**
	 * Returns the userLink between two turtles.
	 * 
	 * @param oneEnd
	 *            an integer
	 * @param otherEnd
	 *            an integer
	 * @return userLink between two turtles
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.UserLink")
	public ungzombies.relogo.UserLink userLink(Number oneEnd, Number otherEnd) {
		return (ungzombies.relogo.UserLink)(this.getNetwork("UserLink").getEdge(turtle(oneEnd),turtle(otherEnd)));
	}

	/**
	 * Returns the userLink between two turtles.
	 * 
	 * @param oneEnd
	 *            a turtle
	 * @param otherEnd
	 *            a turtle
	 * @return userLink between two turtles
	 */
	@ReLogoBuilderGeneratedFor("ungzombies.relogo.UserLink")
	public ungzombies.relogo.UserLink userLink(Turtle oneEnd, Turtle otherEnd) {
		return userLink(oneEnd.getWho(), otherEnd.getWho());
	}

	/**
	 * Returns the value of the global variable numHumans.
	 *
	 * @return the value of the global variable numHumans
	 */
	@ReLogoBuilderGeneratedFor("global: numHumans")
	public Object getNumHumans(){
		return repast.simphony.relogo.ReLogoModel.getInstance().getModelParam("numHumans");
	}

	/**
	 * Sets the value of the global variable numHumans.
	 *
	 * @param value
	 *            a value
	 */
	@ReLogoBuilderGeneratedFor("global: numHumans")
	public void setNumHumans(Object value){
		repast.simphony.relogo.ReLogoModel.getInstance().setModelParam("numHumans",value);
	}

	/**
	 * Returns the value of the global variable numZombies.
	 *
	 * @return the value of the global variable numZombies
	 */
	@ReLogoBuilderGeneratedFor("global: numZombies")
	public Object getNumZombies(){
		return repast.simphony.relogo.ReLogoModel.getInstance().getModelParam("numZombies");
	}

	/**
	 * Sets the value of the global variable numZombies.
	 *
	 * @param value
	 *            a value
	 */
	@ReLogoBuilderGeneratedFor("global: numZombies")
	public void setNumZombies(Object value){
		repast.simphony.relogo.ReLogoModel.getInstance().setModelParam("numZombies",value);
	}

	/**
	 * Returns the value of the global variable gestationPeriod.
	 *
	 * @return the value of the global variable gestationPeriod
	 */
	@ReLogoBuilderGeneratedFor("global: gestationPeriod")
	public Object getGestationPeriod(){
		return repast.simphony.relogo.ReLogoModel.getInstance().getModelParam("gestationPeriod");
	}

	/**
	 * Sets the value of the global variable gestationPeriod.
	 *
	 * @param value
	 *            a value
	 */
	@ReLogoBuilderGeneratedFor("global: gestationPeriod")
	public void setGestationPeriod(Object value){
		repast.simphony.relogo.ReLogoModel.getInstance().setModelParam("gestationPeriod",value);
	}


}