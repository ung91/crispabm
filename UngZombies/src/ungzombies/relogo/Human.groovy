package ungzombies.relogo

import static repast.simphony.relogo.Utility.*
import static repast.simphony.relogo.UtilityG.*

import repast.simphony.relogo.Plural
import repast.simphony.relogo.Stop
import repast.simphony.relogo.Utility
import repast.simphony.relogo.UtilityG
import repast.simphony.relogo.schedule.Go
import repast.simphony.relogo.schedule.Setup
import ungzombies.ReLogoTurtle

class Human extends ReLogoTurtle {
	def infected=false
	def infectionTime = 0
	
	def step() {
		// Assign to winner the patch with the fewest number of Zombies on it.
		def winner=minOneOf(neighbors()){ //neighbors(). in the parentheses, methods can be inserted.
			count(zombiesOn(it)) // p -> count(zombiesOn(p))  into  count(zombiesOn(it)) *Groovy's useful function "it".
		} //Groovy allows to omit the parenthese around a block of code if it's the last argument to a method.
		/*
		 * minOneOf(neighbors(),{
		 * count (zombiesOn(it))
		 * })
		 */
		face(winner)
		forward(1.5)
		if (infected) {
			infectionTime++
			if (infectionTime >=gestationPeriod) {
				hatchZombies(1){
					size=2
				}
				die()
			}
		}
	}
}
