package ungzombies.relogo

import static repast.simphony.relogo.Utility.*;
import static repast.simphony.relogo.UtilityG.*;
import repast.simphony.relogo.Stop;
import repast.simphony.relogo.Utility;
import repast.simphony.relogo.UtilityG;
import repast.simphony.relogo.schedule.Go;
import repast.simphony.relogo.schedule.Setup;
import ungzombies.ReLogoObserver;

class UserObserver extends ReLogoObserver{
	@Setup // this method should be run when the system schedule starts up.
	def setup() {
		clearAll() //Removes all exisitng entities from the ReLogo world and resets the patches to their default state.
		setDefaultShape(Human, "person") //Takes two arguments, 1) a turtle type, 2)string specifying a turtle shape. Available shapes are in the shapes folder in the package explorer.
		createHumans(numHumans){
			setxy(randomXcor(),randomYcor())
		}
		setDefaultShape(Zombie, "zombie")
		createZombies(numZombies){
			setxy(randomXcor(),randomYcor())
			size=2
		}
		
	}
	@Go
	def go() {
		ask(zombies()){ // ask zombie turtle types to execute their step. "ask" is idiom of the Logo. 
			step()
		}
		ask (humans()){
			step()
		}
	}
	
	def remainingHumans() {
		count(humans())
	}
	/**
	 * Add observer methods here. For example:

		@Setup
		def setup(){
			clearAll()
			createTurtles(10){
				forward(random(10))
			}
		}
		
	 *
	 * or
	 * 	
	
		@Go
		def go(){
			ask(turtles()){
				left(random(90))
				right(random(90))
				forward(random(10))
			}
		}

	 */

}